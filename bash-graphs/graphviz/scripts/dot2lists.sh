#!/bin/bash

######################
## HELPER FUNCTIONS ##
######################

function printusage {
	echo "USAGE: "
	echo "$0 inputpath/filename.dot outputpath"
	echo ""
	echo "Creates list-file 'filename.db' corresponding to the given dot-file"
	echo "at outputpath."
}

function aborting {
	echo "ERROR: $1. Exiting."
	echo ""
	printusage
	exit 2
}

function runM4 {
	dotfileM4=$1
	dotfile=$2
	if [[ -f $dotfileM4 ]]; then
		m4 $dotfileM4 > $dotfile
	elif [[ ! -f $ditfileM4 || ! -f $dotfile ]]; then
		aborting "Neither a .dot file nor a .dot.m4 exist"
	fi 
}

function deplist {
	id=$1

	deplist=""
	for dep in $(grep -P "\b->$id(\s|$|//)" $dotfile | sed "s/->.*//" | sed "s|//.*||");	do
		deplist="$deplist, $dep"
	done
	echo "$deplist" | cut -c 2- | sed 's/^ *//g' | sed 's/ *$//g'
}

function writeNode2DB {
	label=$1 
	id=$2
	deplist=$3

	# -e option for correct \n and \t interpretation
	echo -e "node{" >> $dbfile
	echo -e "\tLabel: $label" >> $dbfile
	echo -e "\tID: $id" >> $dbfile
	echo -e "\tDeps: $deplist" >> $dbfile
	echo -e "}\n" >> $dbfile
}

####################
## INPUT HANDLING ##
####################

if [[ $# -ne 2 || ! -d $2 ]]; then
	printusage
	exit 1
fi

if [[ -f $1 && -z $(grep '^digraph ' $1)  ]]; then
	aborting "File $1 is not a digraph file"
fi

###########
## PATHS ##
###########

scriptpath=$(dirname $0)
dotfile=$(readlink -f $1)
dotfileM4=$(echo $dotfile | sed "s|dot|dot.m4|g")
outputdir=$2
dbfile=$outputdir/$(basename $1 .dot).db

##########
## MAIN ##
##########

echo -e "Dependency lists file generated from $dotfile\n" > $dbfile

runM4 $dotfileM4 $dotfile

IFS=$'\n' nodes=($(grep label $dotfile | sed "s|//.*||"))
for node in ${nodes[@]}; do
	label=$(echo $node | sed "s/.*label=\"//" | sed "s/\".*//" | sed 's/\\n/ /')
	shape=$(echo $node | sed "s/.*shape=\"//" | sed "s/\".*//")
	id=$(echo $node | cut -d[ -f1) 

	writeNode2DB $label $id $(deplist $id)
done
