var labelType, useGradients, nativeTextSupport, animate;

function findNode(json, id) {
    var i,
        currentChild,
        result;

    if (id == json.id) {
        return json;
    } else {

        // Use a for loop instead of forEach to avoid nested functions
        // Otherwise "return" will not work properly
        for (i = 0; i < json.children.length; i += 1) {
            dep = json.children[i];

            // Search in the current child
            result = findNode(dep, id);

            // Return the result if the node has been found
            if (result !== false) {
                return result;
            }
        }

        // The node has not been found and we have no more options
        return false;
    }
}

function nearestVisibleParent (nodeId) {
    if (st.graph.hasNode(nodeId)) {
        return nodeId;
    } else {
        return findNode(json, nodeId);
    }
}

function focusNode() {
    lastClicked = st.root;
    lastClickedNodes = JSON.parse(localStorage.getItem("lastClickedNodes"));
    if (lastClickedNodes != null) {
        if (lastClickedNodes.hasOwnProperty(currentTreeId)) {
            if (showHidden) {
                lastClicked = lastClickedNodes[currentTreeId];
            } else {
                lastClicked = st.graph.getNode(nearestVisibleParent(lastClickedNodes[currentTreeId])).data.parent;
            }
        }
    }
    st.onClick(lastClicked);
}

(function() {
  var ua = navigator.userAgent,
      iStuff = ua.match(/iPhone/i) || ua.match(/iPad/i),
      typeOfCanvas = typeof HTMLCanvasElement,
      nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'),
      textSupport = nativeCanvasSupport 
        && (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
  //I'm setting this based on the fact that ExCanvas provides text support for IE
  //and that as of today iPhone/iPad current text support is lame
  labelType = (!nativeCanvasSupport || (textSupport && !iStuff))? 'Native' : 'HTML';
  nativeTextSupport = labelType == 'Native';
  useGradients = nativeCanvasSupport;
  animate = !(iStuff || !nativeCanvasSupport);
})();

var Log = {
  elem: false,
  write: function(text){
    if (!this.elem) 
      this.elem = document.getElementById('log');
    this.elem.innerHTML = text;
    setTimeout(function() {
        document.getElementById('log').innerHTML = "";
      }, 3000)
    let iframeWidth = parseFloat(document.getElementById('iframe').style.width);
    let sliderWidth = parseFloat(document.getElementById('slider_left').style.width)+parseFloat(document.getElementById('slider_right').style.width);
    let treeWidth = parseFloat(document.getElementById('tree').style.width);
    let headlineHeight = parseFloat(document.getElementById('headline').style.height);
    let controlBarHeight = parseFloat(document.getElementById('controlBar').style.height);
    document.getElementById('log').style.left = (iframeWidth + sliderWidth + 0.5 * treeWidth - 0.5 * document.getElementById('log').offsetWidth) + 'px';
    document.getElementById('log').style.top = (headlineHeight + controlBarHeight + 10) + 'px';
  }
}; 
	
	//substitute title, taken from http://stackoverflow.com/questions/11954931/loading-variables-from-external-file-on-server-into-html-doc
	
    function init(animationDuration=0) {
	/*
    var tags = document.getElementsByClassName('templated');
	for (var i=0; i<tags.length; ++i) {
		applyTemplate(tags[i]);
	}
	
	function applyTemplate (tag) {
		for (var key in title) {
			var regexp = new RegExp('%'+key, 'g');
			tag.innerHTML = tag.innerHTML.replace(regexp, title[key]);
		}
	}
	*/
    
    //init Spacetree
    //Create a new ST instance
    
    //window.offsetX = parseFloat(document.getElementById("tree").style.width)/2-100;
    //window.offsetY = 0;
    canvasOffsets = JSON.parse(localStorage.getItem("canvasOffsets"));
    
    if (canvasOffsets != null) {
        if (canvasOffsets.hasOwnProperty(currentTreeId)) {
            window.offsetX = canvasOffsets[currentTreeId].x;
            window.offsetY = canvasOffsets[currentTreeId].y;
        } else {
            window.offsetX = 0;
            window.offsetY = 0;         
        }
    } else {
        window.offsetX = 0;
        window.offsetY = 0;    
    }
    //console.log("Reading coordinates: ", window.offsetX, window.offsetY)
    /*
    console.log(window.rootPosition);
    if (typeof window.rootPosition == "undefined") {
        window.offestTempX = parseFloat(document.getElementById("tree").style.width)/2-100;
        window.offsetTempY = 0;
    } else {
        window.offsetTempX = -1*window.rootPosition.x;
        window.offsetTempY = -1*window.rootPosition.y;
    }
    */

    st = new $jit.ST({
		                
        //levelsToShow: 4-index,
        
        //offsetX: window.offsetX,
        //offsetY: window.offsetY,
        //offsetX: parseFloat(document.getElementById("tree").style.width)/2-100, 
		Events: {  
			enable: true,
			onRightClick: function(node, eventInfo, e) {
                // dirty hack to fix https://gitlab.com/Photon89/kdtg/issues/5, we read the node ID from the event rather than from the node argument which is sometimes wrong
                let Node = st.graph.getNode(e.target.attributes.id.nodeValue);
				if (Node != undefined) {
                    if (removedNodes == null) {
                        removedNodes = [];
                    }
                    
                    if (removedNodes.indexOf(Node.data.uniqueId)>-1) {
                        removedNodes.splice(removedNodes.indexOf(Node.data.uniqueId),1);
                    } else {
                        removedNodes.push(Node.data.uniqueId);
                        if(!showHidden) {
                            st.graph.eachNode( function(node){
                                if(node.data.uniqueId == Node.data.uniqueId) {
                                    st.removeSubtree(node.id, true, 'animate', {
                                        hideLabels: false,
                                    });
                                }
                            });
                        }
                    }
                    localStorage.setItem("removedNodes", JSON.stringify(removedNodes));
                    if (showHidden) {
                        loadNewTree(currentTreeId, 0);
                    }  
				}
			},
            onClick: function(node, eventInfo, e) {
                canvasOffsets = JSON.parse(localStorage.getItem("canvasOffsets"));
                if (canvasOffsets == null) {
                    canvasOffsets = {};
                }
                if (!(currentTreeId in canvasOffsets)) {
                    canvasOffsets[currentTreeId] = {x:0, y:0};
                }
                let newX = /*canvasOffsets[currentTreeId].x +*/ st.canvas.translateOffsetX;
                let newY = /*canvasOffsets[currentTreeId].y +*/ st.canvas.translateOffsetY;
                canvasOffsets[currentTreeId] = {x: newX, y: newY};
                localStorage.setItem("canvasOffsets", JSON.stringify(canvasOffsets));
            }
		},
		
        //id of viz container element
        injectInto: 'infovis',
        //set duration for the animation
        duration: animationDuration,
        //set animation transition type
        transition: $jit.Trans.Quart.easeInOut,
        //set distance between node and its children
        levelDistance: 50,
        //enable panning
        Navigation: {
          enable:true,
          panning:'avoid nodes'
        },
        //set node and edge styles
        //set overridable=true for styling individual
        //nodes or edges
        Node: {
            height: 60,
            width: 140,
            type: 'rectangle',
            color: '#aaa',
            overridable: true,
            //autoHeight: true
        },
        
        Edge: {
            type: 'bezier',
            overridable: true
        },
        
        /*onBeforeCompute: function(node){
            Log.write("Lade " + node.name + ".");
        },
        
        onAfterCompute: function(){
            Log.write("Fertig.");
        },*/
        //This method is called on DOM label creation.
        //Use this method to add event handlers and styles to
        //your node.
        onCreateLabel: function(label, node){
            label.id = node.id;            
            label.innerHTML = node.name;
            
            // The following needs to be done here and not in the Event section to prevent errors...
            label.onclick = function(){
				st.onClick(node.id, {  
                    Move: {  
                      enable: true,  
                      offsetX: 0,
                      offsetY: 0
                    }
                });
                lastClickedNodes = JSON.parse(localStorage.getItem("lastClickedNodes"));
                if (lastClickedNodes == null) {
                    lastClickedNodes = {};
                }
                lastClickedNodes[currentTreeId] = node.id;
                localStorage.setItem("lastClickedNodes", JSON.stringify(lastClickedNodes));
            };

            //set label styles
            var style = label.style;
            style.width = 134 + 'px';
            style.height = 54 + 'px';            
            style.cursor = 'pointer';
            style.color = '#333';
            style.fontSize = '0.8em';
            style.textAlign= 'center';
            style.paddingTop = '3px';
            style.paddingLeft = '3px';	    
        },
        
        //This method is called right before plotting
        //a node. It's useful for changing an individual node
        //style properties before plotting it.
        //The data properties prefixed with a dollar
        //sign will override the global node style properties.
        onBeforePlotNode: function(node){
            //add some color to the nodes in the path between the
            //root node and the selected node.
           /* if (node.selected) {
                node.data.$color = "#ff7";
            }
            else {
                delete node.data.$color;
                //if the node belongs to the last plotted level
                if(!node.anySubnode("exist")) {
                    //count children number
                    var count = 0;
                    node.eachSubnode(function(n) { count++; });
                    //assign a node color based on
                    //how many children it has
                    node.data.$color = ['#aaa', '#baa', '#caa', '#daa', '#eaa', '#faa'][count];                    
                }
                		

            }*/
        },
        
        //This method is called right before plotting
        //an edge. It's useful for changing an individual edge
        //style properties before plotting it.
        //Edge data proprties prefixed with a dollar sign will
        //override the Edge global style properties.
        onBeforePlotLine: function(adj){
           /* if (adj.nodeFrom.selected && adj.nodeTo.selected) {
                adj.data.$color = "#eed";
                adj.data.$lineWidth = 3;
            }
            else {
                delete adj.data.$color;
                delete adj.data.$lineWidth;
            }*/
        },
		onComplete: function() {  
            st.config.duration = 800;
        }  
    });

    //load json data    
    let jsonToShow;
    if (window.treeLoaded) {
        if (showHidden) {
            jsonToShow = jsonShowDeleted;
        } else {
            jsonToShow = jsonHideDeleted;
        }
    } else {
        jsonToShow = {id: "none", name: "Kein Abhängigkeitsbaum geladen, bitte einen Pfeil-Button im Script anklicken!", data: {}, children: []};
    }
    st.loadJSON(jsonToShow);
    //compute node positions and layout
    //st.canvas.clear();
    st.compute();
    //optional: make a translation of the tree
    //st.geom.translate(new $jit.Complex(-200, 0), "current");
    //emulate a click on the root node.
    focusNode();
    st.canvas.translate(window.offsetX, window.offsetY);    
    //st.canvas.translateOffsetX = window.offsetX;
    //st.canvas.translateOffsetY = window.offsetY;
    //console.log(st.canvas.translateOffsetX, st.canvas.translateOffsetY);
    //end
    //Add event handlers to switch spacetree orientation.
    var top = $jit.id('r-top'), 
        left = $jit.id('r-left'), 
        bottom = $jit.id('r-bottom'), 
        right = $jit.id('r-right')
        // normal = $jit.id('s-normal'); Not needed any more, the switch is gone
        
    
    function changeHandler() {
        if(this.checked) {
            top.disabled = bottom.disabled = right.disabled = left.disabled = true;
            st.switchPosition(this.value, "animate", {
                onComplete: function(){
                    top.disabled = bottom.disabled = right.disabled = left.disabled = false;
                }
            });
        }
    };
    
    //top.onchange = left.onchange = bottom.onchange = right.onchange = changeHandler;
    //end
}

