var jsonDB = {
    "node1": {
        "Label": "Die Menge der n-Tupel reeller Zahlen",
        "ID": "n1_1",
        "Deps": null,
        "Type": "NoContent"
    },
    "node2": {
        "Label": "Die Menge der komplexwertigen stetigen Funktionen auf [0,1]",
        "ID": "n1_2",
        "Deps": null,
        "Type": "NoContent"
    },
    "node3": {
        "Label": "Motivation abstrakte Vektorräume 1",
        "ID": "n1_3",
        "Deps": "n1_1, n1_2",
        "Type": "NoContent"
    },
    "node4": {
        "Label": "Notation Körper",
        "ID": "n1_4",
        "Deps": null,
        "Type": "NoContent"
    },
    "node5": {
        "Label": "Definition Vektorraum",
        "ID": "n1_5",
        "Deps": "n1_3, n1_4",
        "Type": "NoContent"
    },
    "node6": {
        "Label": "Motivation abstrakte Vektorräume 2",
        "ID": "n1_6",
        "Deps": "n1_5",
        "Type": "NoContent"
    },
    "node7": {
        "Label": "Eindeutigkeit der Null",
        "ID": "n1_7",
        "Deps": "n1_5",
        "Type": "NoContent"
    },
    "node8": {
        "Label": "Eindeutigkeit der Inversen",
        "ID": "n1_8",
        "Deps": "n1_5",
        "Type": "NoContent"
    },
    "node9": {
        "Label": "Weitere Rechenregeln für Vektoren",
        "ID": "n1_9",
        "Deps": "n1_4, n1_5",
        "Type": "NoContent"
    },
    "node10": {
        "Label": "Notwendigkeit der Vektorraum-Axiome",
        "ID": "n1_10",
        "Deps": "n1_5, n1_4",
        "Type": "NoContent"
    },
    "node11": {
        "Label": "Vereinfachende Notationen für Vektoren",
        "ID": "n1_11",
        "Deps": "n1_4, n1_5",
        "Type": "NoContent"
    },
    "node12": {
        "Label": "Untervektorraum",
        "ID": "n1_12",
        "Deps": "n1_4, n1_5",
        "Type": "NoContent"
    },
    "node13": {
        "Label": "Untervektorraum ist selbst Vektorraum",
        "ID": "n1_13",
        "Deps": "n1_5, n1_12",
        "Type": "NoContent"
    },
    "node14": {
        "Label": "Beispiele für Untervektorräume",
        "ID": "n1_14",
        "Deps": "n1_12",
        "Type": "NoContent"
    },
    "node15": {
        "Label": "R^n und Intuition für abstrakte Vektorräume",
        "ID": "n1_15",
        "Deps": "n1_5",
        "Type": "NoContent"
    },
    "node16": {
        "Label": "Linearkombination und lineare Hülle",
        "ID": "n1_16",
        "Deps": "n1_4, n1_5",
        "Type": "NoContent"
    },
    "node17": {
        "Label": "Lineare Hülle ist Untervektorraum",
        "ID": "n1_17",
        "Deps": "n1_16, n1_12",
        "Type": "NoContent"
    },
    "node18": {
        "Label": "Erzeugendensystem",
        "ID": "n1_18",
        "Deps": "n1_12, n1_16",
        "Type": "NoContent"
    },
    "node19": {
        "Label": "Beispiele für lineare Hüllen und ihre Erzeugendensysteme",
        "ID": "n1_19",
        "Deps": "n1_16, n1_18",
        "Type": "NoContent"
    },
    "node20": {
        "Label": "Lineare Hülle erzeugt von einer (unendlichen) Teilmenge",
        "ID": "n1_20",
        "Deps": "n1_16",
        "Type": "NoContent"
    },
    "node21": {
        "Label": "Beispiel für lineare Hülle einer unendlichen Menge",
        "ID": "n1_21",
        "Deps": "n1_4, n1_20",
        "Type": "NoContent"
    },
    "node22": {
        "Label": "Lineare Unabhängigkeit",
        "ID": "n1_22",
        "Deps": "n1_4, n1_16",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 1,
        "pdfNamedDest": 2
    },
    "node23": {
        "Label": "Lineare Unabhängigkeit eines einzigen Vektors",
        "ID": "n1_23",
        "Deps": "n1_22",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 2,
        "pdfNamedDest": 1
    },
    "node24": {
        "Label": "Lineare Unabhängigkeit Alternativcharakterisierung",
        "ID": "n1_24",
        "Deps": "n1_4, n1_16, n1_22",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 2,
        "pdfNamedDest": 2
    },
    "node25": {
        "Label": "Lineare Unabhängigkeit einer (unendlichen)Teilmenge",
        "ID": "n1_25",
        "Deps": "n1_22, n1_22",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 3,
        "pdfNamedDest": 1
    },
    "node26": {
        "Label": "Beispiel für linear abhängige Vektoren und Mengen",
        "ID": "n1_26",
        "Deps": "n1_24, n1_25, n1_22, n1_4",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 3,
        "pdfNamedDest": 2
    },
    "node27": {
        "Label": "Basis eines Vektorraums",
        "ID": "n1_27",
        "Deps": "n1_4, n1_16, n1_22",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 4,
        "pdfNamedDest": 1
    },
    "node28": {
        "Label": "Basis Charakterisierung durch Eindeutigkeit der Basisentwicklung",
        "ID": "n1_28",
        "Deps": "n1_4, n1_27",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 4,
        "pdfNamedDest": 2
    },
    "node29": {
        "Label": "Kanonische Basis",
        "ID": "n1_29",
        "Deps": "n1_28, n1_4",
        "Type": "NoContent"
    },
    "node30": {
        "Label": "(Unendliche) Teilmenge als Basis",
        "ID": "n1_30",
        "Deps": "n1_27, n1_4, n1_25, n1_20",
        "Type": "NoContent"
    },
    "node31": {
        "Label": "Monome als Basis des Vektorraums der Polynome",
        "ID": "n1_31",
        "Deps": "n1_30, n1_4",
        "Type": "NoContent"
    },
    "node32": {
        "Label": "Existenz der Basis",
        "ID": "n1_32",
        "Deps": "n1_30",
        "Type": "PDF",
        "pdfFileName": "test1.pdf",
        "pdfPageNumber": 1,
        "pdfNamedDest": 1
    },
    "node33": {
        "Label": "Eindeutigkeit der Dimension",
        "ID": "n1_33",
        "Deps": "n1_27",
        "Type": "PDF",
        "pdfFileName": "test1.pdf",
        "pdfPageNumber": 1,
        "pdfNamedDest": 2
    },
    "node34": {
        "Label": "Dimension eines Vektorraums",
        "ID": "n1_34",
        "Deps": "n1_27, n1_30",
        "Type": "external",
        "URL": "www.example.com"
    },
    "node35": {
        "Label": "Maximale Mächtigkeit von Mengen linear unabhängiger Vektoren",
        "ID": "n1_35",
        "Deps": "n1_16, n1_18, n1_22, n1_4",
        "Type": "PDF",
        "pdfFileName": "test1.pdf",
        "pdfPageNumber": 2,
        "pdfNamedDest": 1
    },
    "node36": {
        "Label": "Basisergänzungssatz",
        "ID": "n1_36",
        "Deps": "n1_22, n1_18, n1_27",
        "Type": "PDF",
        "pdfFileName": "test1.pdf",
        "pdfPageNumber": 2,
        "pdfNamedDest": 2
    },
    "node37": {
        "Label": "Basisauswahlsatz",
        "ID": "n1_37",
        "Deps": "n1_27, n1_18, n1_36",
        "Type": "PDF",
        "pdfFileName": "test1.pdf",
        "pdfPageNumber": 3,
        "pdfNamedDest": 1
    },
    "node38": {
        "Label": "n=dim V linear unabhängige Vektoren bilden Basis von V",
        "ID": "n1_38",
        "Deps": "n1_22, n1_27, n1_33, n1_36, n1_34",
        "Type": "PDF",
        "pdfFileName": "test1.pdf",
        "pdfPageNumber": 3,
        "pdfNamedDest": 2
    },
    "node39": {
        "Label": "Dimension eines Unterraums",
        "ID": "n1_39",
        "Deps": "n1_34, n1_12",
        "Type": "external",
        "URL": "www.example.com"
    },
    "node40": {
        "Label": "Schnitt von Unterräumen",
        "ID": "n1_40",
        "Deps": "n1_34, n1_12",
        "Type": "PDF",
        "pdfFileName": "test.pdf",
        "pdfPageNumber": 4,
        "pdfNamedDest": 1
    },
    "node41": {
        "Label": "Summe von Unterräumen",
        "ID": "n1_41",
        "Deps": "n1_12, n1_16",
        "Type": "PDF",
        "pdfFileName": "test1.pdf",
        "pdfPageNumber": 4,
        "pdfNamedDest": 2
    },
    "node42": {
        "Label": "Dimensionsformel für Unterräume",
        "ID": "n1_42",
        "Deps": "n1_12, n1_34, n1_36, n1_27",
        "Type": "PDF",
        "pdfFileName": "test2.pdf",
        "pdfPageNumber": 1,
        "pdfNamedDest": 1
    }
}