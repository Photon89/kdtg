<?php
use \setasign\Fpdi\Fpdi;

require_once(dirname(__DIR__).'/PHP/fpdf181/fpdf.php');
require_once(dirname(__DIR__).'/PHP/fpdi/autoload.php');
require_once(dirname(__DIR__).'/PHP/named_destinations/named_destinations.php');

$PDFpath = realpath(__DIR__ .  '/..') . '/PDF/';
$YAMLfile = dirname(__DIR__) . '/PHP/db.yaml';
$JSONfile = realpath(__DIR__ . '/..') . '/pdf-interface/JIT/includes/db.js';
$arrowRightSVG = realpath(__DIR__ . '/..') . '/pdf-interface/JIT/includes/aiga_right_arrow_bg.png';

$hosting = 'https://photon89.gitlab.io';
//$hosting = 'http://localhost';
$tempHtml = '/kdtg/pdf-interface/temp.html';

$unconvertedPDFdir = $PDFpath . 'pdf';
$convertedPDFdir = $PDFpath . 'pdf-1.4';
$convertActivate = true;

$namedDestsDir =  $PDFpath . 'pdf-nd';
$setNamedDests = true;

$linksSetDir = $PDFpath . 'pdf-links';
$setLinks = true;

class Node {
	
	public function __construct($data) {
		$dataObj = (object) $data;
		$this->ID = $dataObj->ID;
		$this->Label = $dataObj->Label;
		$this->ContentType = $dataObj->Type;
		$this->Deps = array_filter(array_map('trim', explode(',', $dataObj->Deps)));
		if ( $this->ContentType == "PDF") {
			$this->pdfFileName = $dataObj->pdfFileName;
			$this->pdfPageNumber = $dataObj->pdfPageNumber;
			$this->pdfNamedDest = $dataObj->pdfNamedDest;
		}
	}

	public function buildContentUrl($pdfPath) {
		if ( $this->Type == "PDF") {
			return $pdfPath . "/" . $this->pdfFileName . "#nameddest=seite" . $this->pdfPageNumber . "-" . $this->pdfNamedDest;
		} else {
			return 1;
		}
	}

}

function convertToOnePointFour ($unconvertedPDFdir, $convertedPDFdir, $convertActivate) {
	if ($convertActivate == true) {
		$files = scandir($unconvertedPDFdir);
		foreach ($files as &$file) {
		    if (!is_dir($file))
		    {
			$output = shell_exec("/usr/bin/gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$convertedPDFdir/$file $unconvertedPDFdir/$file 2>&1");
			echo $output;
		    }
		}
	}
}

function setNamedDestsInFile ($filenameIn, $filenameOut) {
	ob_start();
	$pdf = new PDF_NamedDestinations();
	$filehandle = fopen($filenameIn, 'rb');
	$pageCount = $pdf->setSourceFile($filehandle);
	for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
	    // import a page
	    $templateId = $pdf->importPage($pageNo);
	    // get the size of the imported page
	    $size = $pdf->getTemplateSize($templateId);
	
	    // create a page (landscape or portrait depending on the imported page size)
	    if ($size["width"] > $size["height"]) {
		$pdf->AddPage('L', array($size["width"], $size["height"]));
	    } else {
		$pdf->AddPage('P', array($size["width"], $size["height"]));
	    }

	    // use the imported page
	    $pdf->useTemplate($templateId);
    
	    // set nameddests
	    $label1 = '#seite' . $pageNo . '-1';
	    $label2 = '#seite' . $pageNo . '-2';
	    $pdf->SetLink($label1, 0, $pageNo);	
	    $pdf->SetLink($label2, 150, $pageNo);

	    /*$pdf->SetFont('Helvetica');
	    $pdf->SetXY(5, 5);
	    $pdf->Write(8, 'A complete document imported with FPDI');*/
	}
	$output = $pdf->Output('F', $filenameOut); # had to create a doc.pdf on server with chmod 777
	fclose($filehandle);
	ob_end_flush();
}

function setNamedDestsInFolder ($inputDir, $outputDir, $setNamedDests) {
	if ($setNamedDests == true) {
		$files = scandir($inputDir);
		foreach ($files as &$file) {
		    if (!is_dir($file))
		    {
			$inputPath = $inputDir . '/' . $file;
			$outputPath = $outputDir . '/' . $file;
			setNamedDestsInFile ($inputPath, $outputPath);
		    }
		}
	}
}

function parseYamlToObjPDFArray ($YAMLfile, $JSONfile) {
	$yamlDB = yaml_parse_file($YAMLfile);

    $json_data = json_encode($yamlDB, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    file_put_contents ( $JSONfile, "var jsonDB = ", LOCK_EX);
    file_put_contents( $JSONfile, $json_data, FILE_APPEND | LOCK_EX);

	$yamlDBarray = array_values($yamlDB);

	$yamlDBObjarray = [];

	foreach ($yamlDBarray as &$node) {
	    $nodeObj = new Node($node);
	    if($nodeObj->ContentType == "PDF") {
		$yamlDBObjarray[$nodeObj->pdfFileName][$nodeObj->pdfPageNumber][$nodeObj->pdfNamedDest]=$nodeObj;
	    }
	}
	
	return $yamlDBObjarray;
}

function setPDFLinksInFolder ($inputDir, $outputDir, $YAMLfile, $JSONfile, $setLinks, $arrowRightSVG, $hosting, $tempHtml) {
	if ($setLinks == true) {
		$yamlDBObjPDFarray = parseYamlToObjPDFArray ($YAMLfile, $JSONfile);
		foreach ( $yamlDBObjPDFarray as $pdfFileName => $nodeObjArray ) {
			$inputPath = $inputDir . '/' . $pdfFileName;
			$outputPath = $outputDir . '/' . $pdfFileName;

			ob_start();
			$pdf = new PDF_NamedDestinations();
			$filehandle = fopen($inputPath, 'rb');
			$pageCount = $pdf->setSourceFile($filehandle);
			for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
			    // import a page
			    $templateId = $pdf->importPage($pageNo);
			    // get the size of the imported page
			    $size = $pdf->getTemplateSize($templateId);
	
			    // create a page (landscape or portrait depending on the imported page size)
			    if ($size["width"] > $size["height"]) {
				$pdf->AddPage('L', array($size["width"], $size["height"]));
			    } else {
				$pdf->AddPage('P', array($size["width"], $size["height"]));
			    }

			    // use the imported page
			    $pdf->useTemplate($templateId);

			    if(array_key_exists($pageNo, $nodeObjArray)) {
				for ($namedDest = 1; $namedDest <= 2; $namedDest++) {
					if(array_key_exists($namedDest, $nodeObjArray[$pageNo])) {
					    // set nameddests
					    $label = '#seite' . $pageNo . '-' . $namedDest;
					    $xPos = 100;
					    $yPos = 150*($namedDest - 1);
					    $pdf->SetLink($label, $yPos, $pageNo);
					    $ID = $nodeObjArray[$pageNo][$namedDest]->ID;
					    $treeLink = $hosting . $tempHtml . '#' . $pdfFileName . '&' . $ID . '&' . $pageNo . '&' . $namedDest;
					    $pdf->Image($arrowRightSVG, $xPos, $yPos, 5, 5, 'PNG', $treeLink); 

					}
				}
			    }
			}
			$output = $pdf->Output('F', $outputPath); # had to create a doc.pdf on server with chmod 777
			fclose($filehandle);
			ob_end_flush();


			//var_dump( $yamlDBObjPDFarray[$pdfFileName]);
			//var_dump( $yamlDBObjPDFarray[$pdfFileName]);
		}
	}
}

convertToOnePointFour ($unconvertedPDFdir, $convertedPDFdir, $convertActivate);
setNamedDestsInFolder ($convertedPDFdir, $namedDestsDir, $setNamedDests);
setPDFLinksInFolder ($namedDestsDir, $linksSetDir, $YAMLfile, $JSONfile, $setLinks, $arrowRightSVG, $hosting, $tempHtml);

/*$pdf->AddPage();
$pdf->SetFont('Arial', '', 14);
$pdf->SetLink('#somelabel', 150, 5);
$pdf->Write(10, 'Link to page 2', '#page-2');

$pdf->AddPage();
$pdf->SetLink('#page-2');
$pdf->Write(10, 'Link to page 1', '#somelabel');*/


?>