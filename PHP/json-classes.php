<?php

//<div style="white-space: pre;">

$YAMLfile = 'db.yaml';
$jsonPath = '../pdf-interface/Graphs/rechenmethoden';
$arrowSVG = 'JIT/includes/aiga_left_arrow_bg.png';
$contentPath = '../PDF/pdf-links';

class Node {
	
	public function __construct($data) {
		$dataObj = (object) $data;
		$this->ID = $dataObj->ID;
		$this->Label = $dataObj->Label;
		$this->Type = $dataObj->Type;
		$this->Deps = array_filter(array_map('trim', explode(',', $dataObj->Deps)));
        
        if ($this->Type == "PDF") {
            $this->URL = "javascript:loadPDF(\"" . $dataObj->pdfFileName . "\", \"" . $dataObj->pdfPageNumber . "\", \"" . $dataObj->pdfNamedDest . "\");";
            $this->Color = "Aquamarine";
            $this->HrefOption = "";
		} else if ($this->Type == "external") {
            if (strpos($dataObj->URL, "http") !== false) {
                $this->URL = $dataObj->URL;
            } else {
                $this->URL = "https://" . $dataObj->URL;
            }
            $this->HrefOption = " target='_blank'";
            $this->Color = "RoyalBlue";
        } else {
            $this->HrefOption = "";
            $this->URL = null;
            $this->Color = "CornflowerBlue";
        }
	}
}

function parseYamlToObjArray ($YAMLfile) {
	$yamlDB = yaml_parse_file($YAMLfile);

	$yamlDBarray = array_values($yamlDB);

	$yamlDBObjarray = [];

	foreach ($yamlDBarray as &$node) {
	    $nodeObj = new Node($node);
	    $yamlDBObjarray[$nodeObj->ID]=$nodeObj;
	}
	
	return $yamlDBObjarray;
}

function generateTreeDataArray ($yamlDBObjarray, $ID, $parentID = "none", $counter = 1, $arrowSVG, $contentPath) {
	$nodeArray = [];
    $currentId = $ID . "_" . $counter;
	$nodeArray['id'] = $currentId; 
    if ($yamlDBObjarray[$ID]->URL === null) {
        $name = $yamlDBObjarray[$ID]->Label;
    } else {
        $name = "<div style='float: left; margin-top: 17px'><a href = '" . $yamlDBObjarray[$ID]->URL . "'" . $yamlDBObjarray[$ID]->HrefOption . "><img src='" . $arrowSVG . "' width='20px'></a></div><div>" . $yamlDBObjarray[$ID]->Label . "</div>";
    }
    $nodeArray['name'] = $name;
    $data = ['uniqueId'=>$ID, '$color'=>$yamlDBObjarray[$ID]->Color, 'parent'=>$parentID];
	$nodeArray['data'] = $data;
	$depsArray = [];
	if ( !empty($yamlDBObjarray[$ID]->Deps) ) {
		foreach ($yamlDBObjarray[$ID]->Deps as $dep) {
			list($depsArray[], $counter) = generateTreeDataArray ($yamlDBObjarray, $dep, $currentId, ++$counter, $arrowSVG, $contentPath);
		}
	}
	$nodeArray['children'] = $depsArray;
	return  array($nodeArray, $counter);
}

function dumpJSONtoFile ($yamlDBObjarray, $ID, $jsonPath, $arrowSVG, $contentPath) {
	$jsonOutFile = $jsonPath . "/" . $ID . ".json";
	file_put_contents ( $jsonOutFile , "var title = {TITLE: \"" . $yamlDBObjarray[$ID]->Label . "\"};\n", LOCK_EX);
	file_put_contents ( $jsonOutFile , "var json = ", FILE_APPEND | LOCK_EX);
	file_put_contents ( $jsonOutFile , json_encode(generateTreeDataArray ($yamlDBObjarray, $ID, "none", 1, $arrowSVG, $contentPath)[0], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT), FILE_APPEND | LOCK_EX);
}

function generateAllJSON ($YAMLfile, $jsonPath, $arrowSVG, $contentPath) {
	$yamlDBObjarray = parseYamlToObjArray ($YAMLfile);
	foreach ( $yamlDBObjarray as $ID => $nodeObj ) {
		dumpJSONtoFile($yamlDBObjarray, $ID, $jsonPath, $arrowSVG, $contentPath);
	}
}

generateAllJSON ($YAMLfile, $jsonPath, $arrowSVG, $contentPath)
//dumpJSONtoFile($yamlDBObjarray, "n1_31", $jsonPath);
//var_dump($yamlDBObjarray["n1_31"]->Deps);
//echo gettype($node1);
//</div>

?>
